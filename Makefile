.PHONY: docker

IMAGE_BASE = jbanetwork/
IMAGE = mariadb
MY_PWD = $(shell pwd)

all: docker

docker:
	docker build -t $(IMAGE_BASE)$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
ifdef PUSH
	docker push $(IMAGE_BASE)$(IMAGE):$(TAG)
endif