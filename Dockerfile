FROM alpine:3.5

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk add --update --no-cache mariadb mariadb-client tzdata gosu@testing bash && \
    mkdir /docker-entrypoint-initdb.d && \
    rm -rf /var/lib/mysql && \
    mkdir -p /var/lib/mysql /var/run/mysqld && \
    chown -R mysql:mysql /var/lib/mysql /var/run/mysqld && \
    chmod 770 /var/run/mysqld && \
    cp /usr/share/zoneinfo/America/New_York /etc/localtime && \
    echo "America/New_York" > /etc/timezone

COPY docker-entrypoint.sh /usr/local/bin/
COPY my.cnf /etc/mysql/my.cnf

RUN chown -R root:mysql /etc/mysql && \
    chmod -R 770 /etc/mysql

ENV TERM dumb

VOLUME /var/lib/mysql
VOLUME /docker-entrypoint-initdb.d

EXPOSE 3306

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["mysqld"]